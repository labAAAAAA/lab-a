# README #

This project jointly develops the README file for a repo.

### What is this repository for? ###

* The project explains the purpose of the readme file and a typical structure.
* Version 10

### How do I get set up? ###

* Members can pull the readme file. Then they can edit the file and commit any changes that they make.
* The typical structure of the readme file should have paragraphs and stuff? 
=======
* Members can pull the readme file. Then make changes and commit them to the repository.


### Contribution guidelines, one paragraph each###

### The purpose of a readme file

* First row: Purpose of the readme file.
A readme file usually accompanies a piece of software once you have downloaded it. The file typically contains 
information about what the application does, what software you may need to run it, what version is being run
and what updates have been included. I may also include warnings and other notices on how to operate the program.
* Second row: typical structure
* Third row: Markdown syntax
* Fourth row: ?

###Markdown
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


### Team Contacts, add yours ###

* katrinhartmann
* Jay Brown
* Declan Gibb
* Darren Ruth
* Jonathan Turner